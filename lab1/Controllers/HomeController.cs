﻿
using lab1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab1.Controllers
{
    public class HomeController : Controller
    {
        
       
        //ArticleContext db = new ArticleContext();
        public ActionResult Index()
        {
            siteEntities1 db = new siteEntities1();
            List<Article> articles = db.Article.ToList<Article>();

            ViewBag.Articles = articles;                    

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}