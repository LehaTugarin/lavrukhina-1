﻿using lab1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace lab1.Controllers
{
    public class DefaultController : Controller
    {
        siteEntities1 db = new siteEntities1();
        //ArticleContext db = new ArticleContext();
        // GET: Default
        public ActionResult Test_View()
        {
            return View();
        }
        public ActionResult Profile()
        {
            return View();
        }
        public ActionResult EditProfile()
        {
            return View();
        }
        public ActionResult AddArticle()
        {
            return View();
        }

        public ActionResult Article(int identity)
        {
            List<Article> articles = db.Article.ToList<Article>();
            foreach (var a in articles)
            {
                if (a.id_article == identity)
                {
                    ViewBag.Article = a;
                    ViewBag.Followed = "Follow";
                    List<Users> users = db.Users.ToList<Users>();

                    foreach (Users u in users)
                    {
                        if (u.id_user == 2)
                        {
                            if (u.fllow.IndexOf(a.id_article.ToString()) >= 0)
                            {
                                ViewBag.Followed = "Followed";
                            }

                        }
                    }
                    Console.WriteLine(a.head);
                }
            }
            return View();
        }

        public ActionResult EditArticle(int identity)
        {
            List<Article> articles = db.Article.ToList<Article>();
            foreach (var a in articles)
            {
                if (a.id_article == identity)
                {
                    ViewBag.Article = a;

                }
            }
            return View();
        }
        public ActionResult AllArticles()
        {
            List<Article> articles = db.Article.ToList<Article>();

            ViewBag.Articles = articles;

            ViewBag.Articles = articles;

            return View();
        }


        public ActionResult AddAction()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddAction(string head, string body, string file)
        {
            Article a = new Article();
            a.head = head;
            a.body = body;
            a.data = DateTime.Now;
            a.image = file;
            a.id_user = 2;

            //db.Article.Add(a);
            db.Database.ExecuteSqlCommand("insert into Article (id_user,head,body,data,image) values(2," +
                "'" + head + "','" + body + "','" + a.data.ToString() + "','" + file + "')");
            db.SaveChanges();
            return Content("Спасобо, скоро опубликуем)");
        }
        public ActionResult EditAction()
        {
            return View();
        }
        [HttpPost]
        public ActionResult EditAction(string head, string body, string file, string id_article)
        {
            Article a = new Article();
            a.head = head;
            a.body = body;
            a.data = DateTime.Now;
            a.image = file;
            a.id_user = 2;

            //db.Article.Add(a);
            db.Database.ExecuteSqlCommand("update Article set head='" + head + "',body='" + body + "',data='" + a.data.ToString() + "',image='" + file + "' where id_article=" + id_article);
            db.SaveChanges();
            return Content("Спасобо, скоро опубликуем)");
        }



        public ActionResult FollowAction()
        {
            return View();
        }
        [HttpPost]
        public ActionResult FollowAction(string type, string id, string file)
        {
            Users user = new Users();
            List<Users> users = db.Users.ToList<Users>();
            foreach (Users u in users)
            {
                if (u.id_user == 2)
                {
                    user = u;
                }
            }
            if (type == "Follow")
            {

                string followList = user.fllow.ToString();
                followList += ',' + id;
                db.Database.ExecuteSqlCommand("update dbo.Users set fllow='" + followList + "' where id_user=2;");
                
                /*db.Database.ExecuteSqlCommand("insert into Article (id_user,head,body,data,image) values(2," +
                "'" + head + "','" + body + "','" + a.data.ToString() + "','" + file + "')");
                return Content("follow)");*/
            }
            if (type == "Followed")
            {
                string followList = user.fllow.ToString();
                followList = followList.Replace((',' + id),"");
                db.Database.ExecuteSqlCommand("update dbo.Users set fllow='" + followList + "' where id_user=2;");
                return Content("Спасобо, скоро удалим подписку)");
            }

            if (type == "Delete")
            {
                string followList = user.fllow.ToString();
                followList.Replace(',' + id, "");
                db.Database.ExecuteSqlCommand("delete from dbo.Article where  id_article=" + id);
                return Content("Спасобо, скоро удалим статью)");
            }


            //db.Article.Add(a);
            /*db.Database.ExecuteSqlCommand("insert into Article (id_user,head,body,data,image) values(2," +
                "'" + head + "','" + body + "','" + a.data.ToString() + "','" + file + "')");
            db.SaveChanges();*/
            return Content("Спасобо, скоро опубликуем)");
        }
    }
}